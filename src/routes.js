import React from "react";
import PropTypes from "prop-types";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import ScrollToTop from "./components/ScrollToTop";
import Enterprises from "./pages/Enterprises";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import ViewEnterprise from "./pages/ViewEnterprise";
import { isAuthenticated } from "./services/auth";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/", state: { from: props.location } }} />
      )
    }
  />
);

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  location: PropTypes.object,
};
PrivateRoute.defaultProps = {
  location: {},
};

export default function Routes() {
  return (
    <BrowserRouter>
      <ScrollToTop>
        <main>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/logout" component={Logout} />
            <PrivateRoute exact path="/empresas" component={Enterprises} />
            <PrivateRoute
              exact
              path="/empresas/:id"
              component={ViewEnterprise}
            />
          </Switch>
        </main>
      </ScrollToTop>
    </BrowserRouter>
  );
}
