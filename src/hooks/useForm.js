import React from "react";

const useForm = () => {
  const [inputs, setInputs] = React.useState({});

  const handleInputChange = (event) => {
    event.persist();
    setInputs((prev) => ({
      ...prev,
      [event.target.name]: event.target.value,
    }));
  };

  return {
    handleInputChange,
    inputs,
  };
};

export default useForm;
