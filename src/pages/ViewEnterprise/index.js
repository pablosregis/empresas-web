import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Navbar from "../../components/Navbar";
import api from "../../services/api";
import "./css/style.scss";

function ViewEnterprise(props) {
  const [response, setResponse] = useState("");
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState("");

  const getEnterprise = async () => {
    await api
      .get(`/enterprises/${props.match.params.id}`)
      .then((res) => {
        setResponse(res.data);
        setLoading(false);
      })
      .catch(() => {
        setError("Ocorreu um erro ao buscar a empresa");
        setLoading(false);
      });
  };

  useEffect(() => {
    document.title = "Empresas - ioasys";
    getEnterprise();
  }, []);

  const { match } = props;

  return (
    <div id="enterprise-view">
      <Navbar
        title={
          (response &&
            response.enterprise &&
            response.enterprise.enterprise_name) ||
          `Empresa ${match.params.id}`
        }
      />

      <div className="container">
        <div className="row">
          <div className="col s12">
            <div className={`box ${isLoading ? "center-align" : ""}`}>
              {isLoading ? (
                <div className="preloader-wrapper small active center-align">
                  <div className="spinner-layer center-align">
                    <div className="circle-clipper left">
                      <div className="circle" />
                    </div>
                    <div className="gap-patch">
                      <div className="circle" />
                    </div>
                    <div className="circle-clipper right">
                      <div className="circle" />
                    </div>
                  </div>
                </div>
              ) : (
                <>
                  {error !== "" ? (
                    error
                  ) : (
                    <div className="row center-align">
                      {response && response.enterprise && (
                        <>
                          {!response.enterprise.photo ? (
                            <div className="no-photo">
                              E{response.enterprise.id}
                            </div>
                          ) : (
                            <img
                              src={
                                "https://source.unsplash.com/random/400x200" ||
                                `${process.env.REACT_APP_API_BASE_URL}${response.enterprise.photo}`
                              }
                              alt="Imagem da empresa"
                            />
                          )}
                        </>
                      )}
                    </div>
                  )}
                  <div className="row">
                    {response && response.enterprise && (
                      <>
                        <div className="col s12">
                          <h1>{response.enterprise.enterprise_name}</h1>
                        </div>
                        <div className="col s12">
                          <hr />
                        </div>
                        <div className="col s12">
                          <p className="description">
                            {response.enterprise.description}
                          </p>
                        </div>
                      </>
                    )}
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

ViewEnterprise.propTypes = {
  match: PropTypes.object.isRequired,
};

export default ViewEnterprise;
