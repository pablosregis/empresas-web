import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Input from "../../components/Input";
import InputPassword from "../../components/Input/InputPassword";
import Button from "../../components/Button";
import logo from "../../assets/img/logo-home@2x.png";

import api from "../../services/api";
import { login, isAuthenticated, logout } from "../../services/auth";

import useForm from "../../hooks/useForm";

import "./css/style.scss";

function Login(props) {
  const { history } = props;
  useEffect(() => {
    document.title = "Empresas - ioasys";
    if (isAuthenticated()) {
      history.push("/empresas");
    }
  }, [history]);

  const [response, setResponse] = useState("");
  const [isLoading, setLoading] = useState("");
  const { inputs, handleInputChange } = useForm();

  const prepareForRequest = () => {
    setLoading(true);
    setResponse("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    prepareForRequest();

    const { email, password } = inputs;
    await api
      .post("/users/auth/sign_in", {
        email,
        password,
      })
      .then((res) => {
        login({
          "access-token": res.headers["access-token"],
          client: res.headers.client,
          uid: res.headers.uid,
        });
        props.history.push("/empresas");
      })
      .catch(() => {
        logout();
        setResponse([
          <div className="answer-error" key="validationError">
            <i className="material-icons answer-icon">error_outline</i>
            Credenciais informadas são inválidas, tente novamente.
          </div>,
        ]);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <div id="login-view">
      <div className="container">
        <div className="col">
          <div className="row">
            <div className="col s12 center-align">
              <img src={logo} alt="Logomarca da empresa ioasys" />
            </div>
            <div className="col s12 center-align">
              <h1>Bem-vindo ao empresas</h1>
            </div>
            <div className="col s12 center-align">
              <p className="lorem-ipsum">
                Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc
                accumsan.
              </p>
            </div>
          </div>

          <form onSubmit={handleSubmit}>
            <div className="row">
              <div className="col s12">
                <>
                  <Input
                    name="email"
                    label="E-mail"
                    value={inputs.email}
                    icon="mail_outline"
                    id="email"
                    type="email"
                    required
                    onChange={handleInputChange}
                  />
                </>
              </div>

              <div className="col s12">
                <>
                  <InputPassword
                    name="password"
                    label="Senha"
                    value={inputs.password}
                    icon="lock_outline"
                    id="password"
                    type="password"
                    required
                    onChange={handleInputChange}
                  />
                </>
              </div>
            </div>

            <div className="row">{response}</div>

            <div className="row">
              <div className="col s12">
                {isLoading && <div id="cover-spin" />}
                <Button name="Entrar" type="submit">
                  Entrar
                </Button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

Login.propTypes = {
  history: PropTypes.object.isRequired,
};
export default Login;
