import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import Navbar from "../../components/Navbar";
import ResponseBox from "../../components/ResponseBox";

import "./css/style.scss";
import api from "../../services/api";

function Enterprises() {
  const [search, toggleSearch] = useState(false);
  const [searchInput, setSearchInput] = useState("");
  // eslint-disable-next-line
  const [error, setError] = useState("");
  const [isLoading, setLoading] = useState(false);
  const [response, setResponse] = useState("teste");

  useEffect(() => {
    document.title = "Empresas - ioasys";
  }, []);

  const getEnterprises = async (searchValue) => {
    setResponse([]);
    setError("");
    setLoading(true);
    await api
      .get(`/enterprises?name=${searchValue}`)
      .then((res) => {
        setLoading(false);
        setResponse(res.data);
      })
      .catch((err) => {
        setLoading(false);
        if (err && err.res && err.res.status && err.res.status === 401) {
          setError([
            <p>
              Sua sessão foi terminada,{" "}
              <NavLink to="/logout">favor realizar login novamente</NavLink>
            </p>,
          ]);
        } else {
          setError([
            <p>Nenhuma empresa foi encontrada para a busca realizada.</p>,
          ]);
        }
        // console.log(error);
      });
  };

  useEffect(() => {
    getEnterprises(searchInput);
  }, [searchInput]);
  return (
    <div id="enterprises-view">
      <Navbar
        title="Empresa"
        isList
        handleFormChange={(e) => setSearchInput(e.target.value)}
        searchActive={search}
        searchValue={searchInput}
        onClick={() => {
          toggleSearch(!search);
          setResponse([]);
        }}
        onClickClear={() => {
          toggleSearch(!search);
        }}
      />
      <>
        {error !== "" ? (
          <div className="init-container valign-wrapper center-align">
            <div className="container">
              <div className="row">
                <div className="col s12">{error}</div>
              </div>
            </div>
          </div>
        ) : (
          <>
            {!search ? (
              <div className="init-container valign-wrapper center-align">
                <div className="container">
                  <div className="row">
                    <div className="col s12">
                      <p>Clique na busca para iniciar</p>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <>
                {isLoading ? (
                  <div className="init-container valign-wrapper center-align">
                    <div className="container">
                      <div className="row">
                        <div className="col s12">
                          <p>
                            <div className="preloader-wrapper small active">
                              <div className="spinner-layer ">
                                <div className="circle-clipper left">
                                  <div className="circle" />
                                </div>
                                <div className="gap-patch">
                                  <div className="circle" />
                                </div>
                                <div className="circle-clipper right">
                                  <div className="circle" />
                                </div>
                              </div>
                            </div>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div className="container">
                    <div className="row">
                      <div className="col s12">
                        {response &&
                          response.enterprises &&
                          response.enterprises.length > 0 &&
                          response.enterprises.map((enterprise, i) => (
                            <ResponseBox key={i} enterprise={enterprise} />
                          ))}
                      </div>
                    </div>
                  </div>
                )}
              </>
            )}
          </>
        )}
      </>
    </div>
  );
}
export default Enterprises;
