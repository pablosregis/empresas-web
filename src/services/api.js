import axios from "axios";
import { getToken } from "./auth";

const api = axios.create({
  baseURL: `${process.env.REACT_APP_PROXY}/${process.env.REACT_APP_API_BASE_URL}/api/${process.env.REACT_APP_API_VERSION}`,
});

api.interceptors.request.use(async (config) => {
  const token = getToken();
  if (token) {
    // eslint-disable-next-line no-param-reassign
    config.headers = JSON.parse(token);
  }
  return config;
});

export default api;
