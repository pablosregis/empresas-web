export const CUSTOM_HEADERS_KEY = "@custom-headers";

export const isAuthenticated = () =>
  localStorage.getItem(CUSTOM_HEADERS_KEY) !== null;

export const getToken = () => localStorage.getItem(CUSTOM_HEADERS_KEY);

export const login = (customHeaders) => {
  localStorage.setItem(CUSTOM_HEADERS_KEY, JSON.stringify(customHeaders));
};

export const logout = () => {
  localStorage.removeItem(CUSTOM_HEADERS_KEY);
};
