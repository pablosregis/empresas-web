import React, { useState } from "react";
const Input = ({
  icon,
  id,
  name,
  type,
  value,
  onChange,
  label,
  isSearchField,
  required
}) => {
  const [active, setActive] = useState(false);
  return (
    <div className="input-field">
      {icon && <i className="material-icons prefix">{icon}</i>}
      <input
        id={id}
        name={name}
        type={type}
        value={value}
        onChange={onChange}
        required={required}
        onFocus={(e) => setActive(true)}
        onBlur={(e) => setActive(false)}
        className={`${isSearchField ? "search" : ""}`}
      />
      <label
        htmlFor={id}
        className={`${active || value !== "" ? "active" : ""}`}
      >
        {label}
      </label>
    </div>
  );
};

export default Input;
