import React, { useState } from "react";
const InputPassword = ({ icon, id, name, value, onChange, label }) => {
  const [active, setActive] = useState(false);
  const [hidden, toggleHide] = useState(true);

  return (
    <div className="input-field">
      {icon && <i className="material-icons prefix">{icon}</i>}
      <input
        id={id}
        name={name}
        type={hidden ? "password" : "text"}
        value={value}
        onChange={onChange}
        required
        onFocus={(e) => setActive(true)}
        onBlur={(e) => setActive(false)}
      />
      <i
        className="material-icons prefix"
        style={{ marginLeft: "-35px", zIndex: "1" }}
        onClick={(e) => {
          toggleHide(!hidden);
        }}
      >
        {hidden ? "visibility_off" : "visibility"}
      </i>
      <label
        htmlFor={id}
        className={`${active || value !== "" ? "active" : ""}`}
      >
        {label}
      </label>
    </div>
  );
};

export default InputPassword;
