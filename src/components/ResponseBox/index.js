import React from 'react';
import { NavLink } from 'react-router-dom';

import './css/style.scss';

const ResponseBox = (enterprise) => {
  return (
    <div id="response-box-component">
      <div className="row box-row">
        <div className="col s3 img">{`E${enterprise.enterprise.id}`}</div>
        <div className="col s9 text">
          <div className="row">
            <div className="col s12 ">
              <NavLink
                className="enterprise-name"
                to={`/empresas/${enterprise.enterprise.id}`}
              >
                {enterprise.enterprise.enterprise_name}
              </NavLink>
            </div>
            <div className="col s12 enterprise-type">
              {enterprise.enterprise.enterprise_type.enterprise_type_name}
            </div>
            <div className="col s12 enterprise-country">
              {enterprise.enterprise.country}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ResponseBox;
