import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import "./css/style.scss";

const Button = (props) => (
  <div id="button-component">
    {props.type === "button" ? (
      <button
        className={`button btn waves-effect `}
        onClick={props.onClick}
        disabled={props.disabled}
      >
        {/* {props.name} */}
        {props.children}
      </button>
    ) : props.type === "submit" ? (
      <button
        className={`button btn waves-effect `}
        type="submit"
        disabled={props.disabled}
      >
        {/* {props.name} */}
        {props.children}
      </button>
    ) : (
      <NavLink to={props.link} className={`button btn waves-effect `}>
        {/* {props.name} */}
        {props.children}
      </NavLink>
    )}
  </div>
);

Button.propTypes = {
  isLoading: PropTypes.bool,
  link: PropTypes.string,
  onClick: PropTypes.func,
  name: PropTypes.string.isRequired,
  type: PropTypes.string
};

export default Button;
