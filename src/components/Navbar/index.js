import React from "react";
import { NavLink } from "react-router-dom";
import Input from "../Input";
import "./css/style.scss";
import logo from "../../assets/img/logo-nav@2x.png";

const Navbar = ({
  isList,
  title,
  onClick,
  onClickClear,
  searchActive,
  searchValue,
  handleFormChange
}) => {
  return (
    <nav id="navbar-component">
      {isList ? (
        <div className="nav-wrapper ">
          {!searchActive ? (
            <React.Fragment>
              <NavLink to="/" className="brand-logo center">
                <img src={logo} alt="Logo da empresa ioasys" />
              </NavLink>
              <ul id="nav-mobile" className="right">
                <li>
                  <button onClick={onClick}>
                    <i className="material-icons">search</i>
                  </button>
                </li>
              </ul>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div className="brand-logo center">
                <Input
                  name="searchValue"
                  label="Pesquisar"
                  value={searchValue}
                  icon="search"
                  id="searchValue"
                  type="text"
                  onChange={handleFormChange}
                  isSearchField
                  className="search"
                />
              </div>
              <ul id="nav-mobile" className="right">
                <li>
                  <button onClick={onClickClear}>
                    <i className="material-icons">clear</i>
                  </button>
                </li>
              </ul>
            </React.Fragment>
          )}
        </div>
      ) : (
        <div className="nav-wrapper">
          <a href="#!" className="brand-logo title-enterprise center">
            {title}
          </a>
          <ul id="nav-mobile" className="left">
            <li>
              <NavLink to="/empresas">
                <i className="material-icons">arrow_back</i>
              </NavLink>
            </li>
          </ul>
        </div>
      )}
    </nav>
  );
};

export default Navbar;
