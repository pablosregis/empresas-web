# Projeto Empresas

Projeto de teste para seleção de vaga

## Iniciando

Baixe o repositório do projeto.

### Pré-requisitos

```
node
```

### Instalação

Instale as dependências do projeto

```
npm install
```

ou

```
yarn
```

Rode o servidor. (Irá iniciar em http://localhost:3000/)

```
npm start
```

ou

```
yarn start
```
